{
    function foo () { console.log(1) }
    foo() === 1
    {
        function foo () { console.log(2) }
        foo() === 2
    }
    foo() === 1
}