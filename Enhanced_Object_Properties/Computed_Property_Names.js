// Enhanced Object Properties
// Computed Property Names
// Support for computed names in object property definitions

let quux = () => ` foo`;

let obj2 = {
  foo: "bar",
  ["baz" + quux()]: 42
};

console.log(`Value obj2:`, JSON.stringify(obj2))