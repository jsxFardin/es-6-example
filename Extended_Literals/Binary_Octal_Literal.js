// Extended Literals
// Binary & Octal Literal
// Direct support for safe binary and octal literals.

console.log('0b111110111 === 503',0b111110111 === 503);
console.log(`Compare octal 0o767 === 503:`, 0o767 === 503);