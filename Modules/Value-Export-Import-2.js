// Modules
// Value Export/Import
// Support for exporting/importing values from/to modules without global namespace pollution.

// import * as math from "./Export_Import";
import { sum, pi } from "./Export_Import";

console.log("2π = ", math.sum(math.pi, math.pi));
console.log("2π = ", sum(pi, pi));