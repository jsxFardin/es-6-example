// symbol is a new data type added in es6 
const symbol1 = Symbol();
const symbol2 = Symbol(42);
const symbol3 = Symbol('foo');

// console.log(typeof symbol1);
// expected output: "symbol"

// console.log(symbol3.toString());
// expected output: "Symbol(foo)"

// console.log(Symbol('foo') === Symbol('foo'));
// expected output: false

var sym = Symbol('foo');
console.log(typeof sym);     // "symbol" 
var symObj = Object(sym);
console.log(typeof symObj);  // "object"
