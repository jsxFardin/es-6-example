// create an ArrayBuffer with a size in bytes
var buffer = new ArrayBuffer(16);

if (buffer.byteLength === 16) {
    console.log("length is 16 bytes");
} else {
    console.log("wrong");
}
// ========
var int32View = new Int32Array(buffer);
for (var i = 0; i < int32View.length; i++) {
    console.log('Int32Array ' + i + ': ' + int32View[i]);
}
console.log('\n');
// ==========
var int16View = new Int16Array(buffer);
for (var i = 0; i < int16View.length; i++) {
    console.log('Int16Array ' + i + ': ' + int16View[i]);
}
console.log('\n');
