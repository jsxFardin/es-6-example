// Promises
// Promise Usage
// First class representation of a value that may be made asynchronously and be available in the future

function msgAfterTimeout(msg, who, timeout) {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(`${msg} Hello ${who}!`), timeout);
    });
  }
  
  msgAfterTimeout('', 'Foo', 100).then((msg) =>
    msgAfterTimeout(msg, 'Bar', 200)
  ).then((msg) => {
    console.log(`Response promise after 300ms:`, msg);
  });