// Promise Combination
// Combine one or more promises into new promises without having to take care of ordering 
// of the underlying asynchronous operations yourself.

let fetchPromised = (name, timeout) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => resolve(`Hi ${name}`), timeout);
    });
}

Promise.all([
    fetchPromised('Foo', 1000),
    fetchPromised('Bar', 500),
    fetchPromised('Baz', 200)
]).then((data) => {
    let [foo, bar, baz] = data;
    console.log(`Response all promises: foo=${foo} bar=${bar} baz=${baz}`);
}, (err) => {
    console.log(`Error: ${err}`);
});