let nameShow = (name, time) => {
    return new Promise((resolve, reject) => {
        if (isNaN(time)) {
            reject( new Error("enter number"));
        }
        setTimeout(() => resolve(`Hi ${name}`), time);
    })
}

nameShow("fardin", 1000)
.then((data) => {
    console.log(data);
})
.catch((errr)=>{
    console.error(errr);
})