// Proxying
// Hooking into runtime-level object meta-operations.

let target = {
    foo: 'Welcome, foo'
};
let proxy = new Proxy(target, {
    get(receiver, name) {
        return name in receiver ? receiver[name] : `Hello, ${name}`;
    }
});
console.log(`proxy.foo:`, proxy.foo);
console.log(`proxy.world:`, proxy.world);