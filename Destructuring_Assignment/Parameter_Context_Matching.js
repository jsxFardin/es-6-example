// Destructuring Assignment
// Parameter Context Matching
// Intuitive and flexible destructuring of Arrays and Objects into individual parameters during function calls.

function arrayTest([name,age]){
    console.log(`i am ${name}.and i am ${age} old.`);
}

arrayTest(['array',21]);

function objectsTest({name,age}){
    console.log(`i am ${name}.and i am ${age} old.`)
}
objectsTest({name:'Object',age:21});