// Destructuring Assignment
// Array Matching
// Intuitive and flexible destructuring of Arrays into individual variables during assignment.
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment
var num = [1,2,3];

var [a,,b] = num;

console.log(a);
console.log(b);

[a,b] = [b,a]; // swapi

console.log(a);
console.log(b);