// Fail-Soft Destructuring
// Fail-soft destructuring, optionally with defaults.

var list = [1, 2,7];    // main array
var [a = 7, b = 42, c = 4, d] = list; // asign and tring to re-write data
var [a = 7, b = 42, c = 3, d=0] = list;
console.log(`Compare a === 7:`, a === 7);
console.log(`Compare b === 42:`, b === 42);
console.log(`Compare c === 3:`, c === 3);
console.log(`Compare d === undefined:`, d === undefined);


// if you want, then you will add element to array. but you don't change value of any array