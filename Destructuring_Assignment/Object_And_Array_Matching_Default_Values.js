// Destructuring Assignment
// Object And Array Matching, Default Values
// Simple and intuitive default values for destructuring of Objects and Arrays.

var x = [1, 2, 3, 4, 5];
var [y, z, a, b, c] = x;
// console.log(y); // 1
// console.log(z); // 2
// console.log(a); // 3
// console.log(b); // 4
// console.log(c); // 5
//==================================
var a, b;

[a = 5, b = 7] = [1, 3];
// console.log(a); // 1
// console.log(b); // 3
// =====================================
var obj = {
    a: 1
};
var {
    a,
    b = 2
} = obj;

// console.log(`Value a:`, a);
// console.log(`Value b:`, b);

// ================================

var list = [1];
var [x, y = 2] = list;

// console.log(`Value x:`, x);
// console.log(`Value y:`, y);
//========================================

/*
    In this example, f() returns the values [1, 2] as its output,
     which can be parsed in a single line with destructuring.
*/
function f() {
    return [1, 2];
}

var a, b;
[a, b] = f();
// console.log(a); // 1
// console.log(b); // 2

// =======================================

var obj = { a: 1 }
var list = [ 1 ]
var { a, b = 2 } = obj
var [ x, y = 2 ] = list

console.log(x);