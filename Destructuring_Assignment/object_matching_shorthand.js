// Object Matching, Shorthand Notation
// Intuitive and flexible destructuring of Objects into individual variables during assignment.

function getASTNode() {
  return {op: 1, lhs: 2, rhs: 3};
};

var {op, lhs, rhs} = getASTNode();

console.log(`Value op:`, op);
console.log(`Value lhs:`, lhs);
console.log(`Value rhs:`, rhs);


// object pattern matching
let {lName, fName} = {fName: 'John', age: 15, lName: 'Doe'};

// array pattern matching
let [first, second, third] = [8, 4, 100, -5, 20];

// output: Doe, John
console.log(lName + ', '+ fName);

// output: 100, 4, 8
console.log(third, second, first);