// Object Matching, Deep Matching
// Intuitive and flexible destructuring of Objects into individual variables during assignment.

function getNotation(){
    return {
        df:1,
        ef:2,
        ff:{
            gf: 3
        },
        hf: 4,
        if: 5
    }
}
var {df:a,ef:b,ff:{gf:c},hf:d,if:e} = getNotation();

console.log(a);
console.log(b);
console.log(c);
console.log(d);
console.log(e);

// function getASTNode2() {
//     return {op: 1, lhs: {op: 2}, rhs: 3};
//   };
  
//   var {op: a, lhs: {op: b}, rhs: c} = getASTNode2();