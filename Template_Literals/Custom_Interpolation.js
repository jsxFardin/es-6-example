// Template Literals
// Custom Interpolation
// Flexible expression interpolation for arbitrary methods.
// https://css-tricks.com/template-literals/
function get(array, param1, param2) {
    console.log(`Value params get():`, JSON.stringify(array), param1, param2);
    let url = `${array[0]}${param1}${array[1]}${param2}`;
    console.log(`Value url:`, url);
}

var bar = 10;
var baz = 20;
var quux = 'dsjflj';

let custom_interpolation = get `http://example.com/foo?bar=${bar + baz}&quux=${quux}`;

// console.log(custom_interpolation);