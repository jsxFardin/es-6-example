// Extended Parameter Handling
// Default Parameter Values
// Simple and intuitive default values for function parameters.

function f (x, y = 7, z = 42) {
    return x + y + z;
}
console.log(f(11));
