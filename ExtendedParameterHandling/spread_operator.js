// Extended Parameter Handling
// Spread Operator
// Spreading of elements of an iterable collection (like an array or even a string) into both literal elements and individual function parameters.


// var params = [ "hello", true, 7 ];
// var other = [ 1, 2, ...params ]; // [ 1, 2, "hello", true, 7 ]

// function f (x, y, ...a) {
//     return (x + y) * a.length;
// }
// f(1, 2, ...params) === 9;

// var str = "foo"; // if you give only a string? then it will spread as an array .
// var chars = [ ...str ]; // [ "f", "o", "o" ]

function function2(x, y, ...a) {
    return (x + y) * a.length;
  }
  
  let params = ["hello", true, 7];
  console.log(`Compare function2() === 9:`, function2(1, 2, ...params) === 9);
  let other = [1, 2, ...params];
  console.log(`Array other:`, JSON.stringify(other));
  
  let str = "foo";
  let chars = [...str];
  console.log(`Array chars:`, JSON.stringify(chars));
