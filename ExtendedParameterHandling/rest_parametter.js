// Extended Parameter Handling
// Rest Parameter
// Aggregation of remaining arguments into single parameter of variadic functions.

function resetFunc(x,...a){
    return x  * a.length;
}

// console.log(resetFunc(1,'fardin',true,40));

function myFun(a, b, ...manyMoreArgs) {
    console.log("a", a); 
    console.log("b", b);
    console.log("manyMoreArgs", manyMoreArgs); 
  }
  
//   myFun("one", "two", "three", "four", "five", "six");

function f (x, y, ...a) {
    return (x + y) * a.length;
}
console.log(f(1, 2, "hello", 'jkdfjd','df',4,70,90,989));


function add(...args) {
    let result = 0;
  
    for (let arg of args) result += arg;
  
    return result
  }
  
  add(1) // returns 1
  add(1,2) // returns 3
  add(1, 2, 3, 4, 5) // returns 15

//   Note: Rest parameters have to be at the last argument. This is because it collects all remaining/ 
// excess arguments into an array. So having a function definition like this does not make sense and it errors out. :