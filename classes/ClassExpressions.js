// A class expression is another way to define a class. Class expressions can be named or unnamed.
//  The name given to a named class expression is local to the class's body.
//   (it can be retrieved through the class's (not an instance's) name property, though).

// unnamed
let UnNamed = class {
    constructor(height, width) {
        this.height = height;
        this.width = width;
    }
};
console.log(UnNamed.name);
// output: "Rectangle"

// named
let Named = class Rectangle2 {
    constructor(height, width) {
        this.height = height;
        this.width = width;
    }
};
console.log(Named.name);
// output: "Rectangle2"