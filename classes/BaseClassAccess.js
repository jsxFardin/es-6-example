class Shape{
    constructor(id, x, y){
        this.id = id;
        this.x = x;
        this.y = y;
    }
    toString(){
        return `Shape(${this.id})`;
    }
}
class Rectangle extends Shape{
    constructor(id, x, y, width, height){
        super(id, x, y);
    }
    toString(){
        return `Rectangle > ${super.toString()}`;
    }
}
class Circle extends Shape{
    constructor(id, x, y, rdius){
        super(id, x, y);
    }
    toString(){
        return `Circle > ${super.toString()}`;
    }
}

let shape = new Shape(1, 10, 20);
console.log(shape.toString());
let rectangle = new Rectangle(2, 20, 30);
console.log(rectangle.toString());
let circle = new Circle(3, 30, 40);
console.log(circle.toString());