class Shape{
    constructor(id, x, y){
        this.id = id;
        this.x = x;
        this.y = y;
    }
    toString(){
        return `Shape(${this.id})`;
    }
}

class Rectangle extends Shape {
    constructor(id, x, y, width, height) {
      super(id, x, y);
      this.width = width;
      this.height = height;
    }
    static defaultRectangle() {
      return new Rectangle('default', 2, 2, 100, 100);
    }
  }
  class Circle extends Shape {
    constructor(id, x, y, radius) {
      super(id, x, y);
      this.radius = radius;
    }
    static defaultCircle() {
      return new Circle('default', 4, 4, 100);
    }
  }
  
  let rectangle = Rectangle.defaultRectangle();
  console.log(`Rectangle data:`, JSON.stringify(rectangle));
  let circle = Circle.defaultCircle();
  console.log(`Circle data:`, JSON.stringify(circle));