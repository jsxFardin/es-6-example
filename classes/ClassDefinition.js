// Classes
// Class Definition
// More intuitive, OOP-style and boilerplate-free classes

class Shape {
    constructor(id, x, y) {
      this.id = id;
      this.move(x, y);
    }
    move(x, y) {
      this.x = x;
      this.y = y;
    }
    getPos() {
      return {
        x: this.x,
        y: this.y
      };
    }
  }
  
  let shape = new Shape(1, 10, 20);
  console.log(`Shape pos:`, JSON.stringify(shape.getPos()));
  shape.move(15, 35);
  console.log(`Shape pos:`, JSON.stringify(shape.getPos()));