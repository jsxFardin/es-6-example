// Number Sign Determination
// Determine the sign of a number, including special cases of signed zero and non-number.
// The Math.sign() function returns the sign of a number, indicating whether the number is positive, negative or zero.

// Math.sign(x)
/*Return valueSection
A number representing the sign of the given argument. 
If the argument is a positive number, negative number, 
positive zero or negative zero, the function will return 1, -1, 0 or -0 respectively. Otherwise, NaN is returned.*/

console.log(Math.sign(3));
// expected output: 1

console.log(Math.sign(-3));
// expected output: -1

console.log(Math.sign(0));
// expected output: 0

console.log(Math.sign('-3'));
// expected output: -1
