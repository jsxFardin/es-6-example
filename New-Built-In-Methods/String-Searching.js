// String Searching
// New specific string functions to search for a sub-string.
// arr.includes(searchElement[, fromIndex]) || it returns boolean.
// str.startsWith(searchString[, position]) || it returns boolean.
//str.endsWith(searchString[, length]) || it returns boolean.

console.log(`'hello'.startsWith('ello', 1):`, 'hello'.startsWith('ello', 1));
console.log(`'hello'.endsWith('hell', 4):`, 'hello'.endsWith('hell', 4));
console.log(`'hello'.includes('ell'):`, 'hello'.includes('ell'));
console.log(`'hello'.includes('ell', 1):`, 'hello'.includes('ell', 1));
console.log(`'hello'.includes('ell', 2):`, 'hello'.includes('ell', 2));