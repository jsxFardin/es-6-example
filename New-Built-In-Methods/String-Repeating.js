// String Repeating
// New string repeating functionality.


let repeat1 = 'f'.repeat(4 * 2);
let repeat2 = 'foo'.repeat(3);
console.log(`' '.repeat(4 * 2):`, repeat1);
console.log(`'foo'.repeat(3):`, repeat2);