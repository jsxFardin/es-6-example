// Number Comparison
// Availability of a standard Epsilon value for more precise comparison of floating point numbers.

let num_comp1 = (0.1 + 0.2 === 0.3);
let num_comp2 = (Math.abs((0.1 + 0.2) - 0.3) < Number.EPSILON);
console.log(`0.1 + 0.2 === 0.3:`, num_comp1);
console.log(`Math.abs((0.1 + 0.2) - 0.3) < Number.EPSILON:`, num_comp2);