// Number Safety Checking
// Checking whether an integer number is in the safe range, i.e., 
// it is correctly represented by JavaScript (where all numbers, including integer numbers, 
// are technically floating point number).

let isSafety1 = Number.isSafeInteger(42);
let isSafety2 = Number.isSafeInteger(9007199254740992);
console.log(`Number.isSafeInteger(42):`, isSafety1);
console.log(`Number.isSafeInteger(9007199254740992):`, isSafety2);