// Array Element Finding
// New function for finding an element in an array.

let find = [1, 3, 4, 6].find(x => x > 3);
// console.log(`Find value x > 3:`, find);

var inventory = [
    {name: 'apples', quantity: 2},
    {name: 'bananas', quantity: 0},
    {name: 'cherries', quantity: 5}
];

function isCherries(fruit) { 
    return fruit.name === 'bananas';
}

console.log(inventory.find(isCherries)); 
// { name: 'cherries', quantity: 5 }
