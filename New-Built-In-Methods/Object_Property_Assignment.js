// Object Property Assignment
// New function for assigning enumerable properties of one or more source objects onto a destination object.

var dst = {
    quux: 0
};
var src1 = {
    foo: 1,
    bar: 2
};
var src2 = {
    foo: 3,
    baz: 4
};
const vla = Object.assign(dst, src1, src2);
console.log(vla);
// console.log(`Value dst.quux:`, dst.quux);
// console.log(`Value dst.foo:`, dst.foo);
// console.log(`Value dst.bar:`, dst.bar);
// console.log(`Value dst.baz:`, dst.baz);