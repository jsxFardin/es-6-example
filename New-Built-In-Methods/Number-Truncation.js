// Number Truncation
// Truncate a floating point number to its integral part, completely dropping the fractional part.

// Math.trunc(x) || Return valueSection : The integer part of the given number.

console.log(`Math.trunc(42.7):`, Math.trunc(42.7));
console.log(`Math.trunc( 0.1):`, Math.trunc(0.1));
console.log(`Math.trunc(-0.1):`, Math.trunc(-0.1));