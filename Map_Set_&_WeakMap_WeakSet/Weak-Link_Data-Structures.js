let isMarked = new WeakSet();
let attachedData = new WeakMap();

class Node {
  constructor(id) {
    this.id = id;
  }
  mark() {
    isMarked.add(this);
  }
  unmark() {
    isMarked.delete(this);
  }
  marked() {
    return isMarked.has(this);
  }
  set data(data) {
    attachedData.set(this, data);
  }
  get data() {
    return attachedData.get(this);
  }
}

let foo = new Node('foo');

console.log(`Value foo:`, JSON.stringify(foo));
foo.mark();
foo.data = "bar";
console.log(`foo.data === "data":`, foo.data === 'bar');
console.log(`Value foo:`, JSON.stringify(foo));
console.log(`foo isMarked:`,isMarked.has(foo));
console.log(`attached data foo:`, attachedData.has(foo));
foo = null /* remove only reference to foo */
console.log(`attached data foo:`, attachedData.has(foo));
console.log(`foo isMarked:`, isMarked.has(foo));