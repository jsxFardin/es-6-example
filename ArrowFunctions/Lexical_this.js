
//  Lexical This
//  More intuitive handling of current object context.

// let nums = [10,101,200,254];
// let fives = [];

// this.nums.forEach(v => {
//     if ( v % 5 === 0){
//         this.fives.push(v);
//     }
// });
var example ={
    nums : [10,101,200,254],
    fives : [],
    getFives : function(){
        this.nums.forEach((v) => {
            if (v % 5 === 0)
                this.fives.push(v);
                console.log(this.fives)
        })
    } 
}

example.getFives();


