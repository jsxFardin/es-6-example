let fibonacci = {
    [Symbol.iterator]() {
        let pre = 0,
            cur = 1;
        return {
            next() {
                [pre, cur] = [cur, pre + cur];
                return {
                    done: false,
                    value: cur
                };
            }
        };
    }
}

for (let n of fibonacci) {
    if (n > 10000)
        break;
    console.log(`Current value fibonacci:`, n);
}