// Enhanced Regular Expression
// Regular Expression Sticky Matching
// Keep the matching position sticky between matches and this way support efficient parsing of arbitrary long input strings,
//  even with an arbitrary number of distinct regular expressions.

let message = "My name is Foo and my lastname is Bar."
let pattern = /my [a-zA-Z]+/y;

console.log(`Return pattern /my [a-zA-Z]+/y:`, JSON.stringify(pattern.exec(message)));

pattern.lastIndex = 19; // The lastIndex property specifies the index at which to start the next match.

console.log(`Return pattern /my [a-zA-Z]+/y:`, JSON.stringify(pattern.exec(message)));
console.log(`Return pattern /my [a-zA-Z]+/y:`, JSON.stringify(pattern.exec(message)))