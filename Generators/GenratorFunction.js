function* range(start, end, step) {
    while (start < end) {
        yield start;
        start += step;
    }
}
function test(start, end, step) {
    while (start < end) {
        let start;
        start += step;
    }
}

for (let i of test(0, 10, 2)) {
    console.log(`Current value range:`, i);
}
// for (let i of range(0, 10, 2)) {
//     console.log(`Current value range:`, i);
// }