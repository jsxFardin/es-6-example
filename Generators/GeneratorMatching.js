let fibonacci = function* (numbers) {
    let pre = 0,
        cur = 1;
    while (numbers-- > 0) {
        [pre, cur] = [cur, pre + cur];
        yield cur;
    }
};

for (let n of fibonacci(10)) {
    console.log(`Current value fibonacci:`, n);
}

let numbers = [...fibonacci(10)];
//   console.log(`Value numbers:`, JSON.stringify(numbers));
let [n1, n2, n3, ...others] = fibonacci(10);
//   console.log(`Values n1, n2, n3, others:`, n1, n2, n3, others);