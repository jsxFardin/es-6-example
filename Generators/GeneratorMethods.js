class Clz {
    * bar() {
        console.log('Hello bar');
    }
};
let Obj = {
    * foo() {
        console.log('Hello foo');
        yield 1;
        console.log('Bye foo');
        yield 2;
        return "finish!";
    }
};

let clz = new Clz();
// console.log(`Clz bar:`, JSON.stringify(clz.bar().next()));

let obj = Obj.foo();
// console.log(`Obj.foo:`, JSON.stringify(Obj.foo().next()));
console.log(`Obj.foo:`, JSON.stringify(obj.next()));
console.log(`Obj.foo:`, obj.next());